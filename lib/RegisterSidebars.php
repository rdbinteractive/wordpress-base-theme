<?php
/**
 * Class RegisterSidebars
 *
 * @package IETech\ThemeSetup
 */

namespace IETech\ThemeSetup;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

/**
 * Class RegisterSidebars
 *
 * @package IETech\ThemeSetup
 */
class RegisterSidebars
{

    /**
     * Register all sidebars.
     */
    public function registerSidebars()
    {
        add_action('widgets_init', array($this, 'registerPrimarySidebar'));
        add_action('widgets_init', array($this, 'registerContactSidebar'));
        add_action('widgets_init', array($this, 'registerReferenceSidebar'));
        add_action('widgets_init', array($this, 'registerSingleReferenceSidebar'));
        add_action('widgets_init', array($this, 'registerTrainingSidebar'));
        add_action('widgets_init', array($this, 'registerPartnerSidebar'));
        add_action('widgets_init', array($this, 'registerEventSidebar'));
    }

    /**
     * Create a primary, default sidebar.
     */
    public function registerPrimarySidebar()
    {
        register_sidebar(array(
            'id'            => 'primary-sidebar',
            'name'          => __('Primary Sidebar', 'ieTech_theme'),
            'description'   => __('The default sidebar', 'ieTech_theme'),
            'before_widget' => '<div class="SidebarCtaWidget">',
            'after_widget'  => '</div>',
            'before_title'  => '<h1 class="SidebarCtaWidget__title">',
            'after_title'   => '</h1>',
        ));
    }

    public function registerContactSidebar()
    {
        register_sidebar(array(
            'id'            => 'contact-sidebar',
            'name'          => __('Contact Page Sidebar', 'ieTech_theme'),
            'description'   => __('Shown on Contact Page', 'ieTech_theme'),
            'before_widget' => '<div class="SidebarCtaWidget">',
            'after_widget'  => '</div>',
            'before_title'  => '<h1 class="SidebarCtaWidget__title">',
            'after_title'   => '</h1>',
        ));
    }

    public function registerReferenceSidebar()
    {
        register_sidebar(array(
            'id'            => 'reference-sidebar',
            'name'          => __('Vendor Reference Library Sidebar', 'ieTech_theme'),
            'description'   => __('Shown on Vendor Reference Library page', 'ieTech_theme'),
            'before_widget' => '<div class="SidebarCtaWidget">',
            'after_widget'  => '</div>',
            'before_title'  => '<h1 class="SidebarCtaWidget__title">',
            'after_title'   => '</h1>',
        ));
    }

    public function registerSingleReferenceSidebar()
    {
        register_sidebar(array(
            'id'            => 'single-reference-sidebar',
            'name'          => __('Single Reference Sidebar', 'ieTech_theme'),
            'description'   => __('Shown on single Reference item pages', 'ieTech_theme'),
            'before_widget' => '<div class="SidebarCtaWidget">',
            'after_widget'  => '</div>',
            'before_title'  => '<h1 class="SidebarCtaWidget__title">',
            'after_title'   => '</h1>',
        ));
    }

    public function registerTrainingSidebar()
    {
        register_sidebar(array(
            'id'            => 'training-sidebar',
            'name'          => __('Training Page Sidebar', 'ieTech_theme'),
            'description'   => __('Shown on Training Video Page', 'ieTech_theme'),
            'before_widget' => '<div class="SidebarCtaWidget">',
            'after_widget'  => '</div>',
            'before_title'  => '<h1 class="SidebarCtaWidget__title">',
            'after_title'   => '</h1>',
        ));
    }

    public function registerPartnerSidebar()
    {
        register_sidebar(array(
            'id'            => 'partner-sidebar',
            'name'          => __('Partner Page Sidebar', 'ieTech_theme'),
            'description'   => __('Shown on Partner Page', 'ieTech_theme'),
            'before_widget' => '<div class="SidebarCtaWidget">',
            'after_widget'  => '</div>',
            'before_title'  => '<h1 class="SidebarCtaWidget__title">',
            'after_title'   => '</h1>',
        ));
    }

    public function registerEventSidebar()
    {
        register_sidebar(array(
            'id'            => 'event-sidebar',
            'name'          => __('Event Page Sidebar', 'ieTech_theme'),
            'description'   => __('Shown on Event Page', 'ieTech_theme'),
            'before_widget' => '<div class="SidebarCtaWidget">',
            'after_widget'  => '</div>',
            'before_title'  => '<h1 class="SidebarCtaWidget__title">',
            'after_title'   => '</h1>',
        ));
    }
}
