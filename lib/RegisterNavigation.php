<?php

namespace IETech\ThemeSetup;

use IETech\ThemeSetup\Lib\PrimaryNavWalker;
use IETech\ThemeSetup\Lib\SocialNavWalker;

/**
 * Class RegisterNavigation
 *
 * @package IETech\ThemeSetup
 */
class RegisterNavigation
{

    public function init()
    {
        add_action('after_setup_theme', array($this, 'registerPrimaryNav'));
        // add_filter('nav_menu_link_attributes', array($this, 'navAttr'), 10, 4);
    }

    public function ieTechPrimaryNav()
    {
        wp_nav_menu(array(
            'container'      => false,
            'menu'           => __('Primary Navigation', 'ieTech_theme'),
            'menu_class'     => 'PrimaryNavigation jsPrimaryNavigation',
            'theme_location' => 'primary-nav',
            'depth'          => 0,
            'fallback_cb'    => '__return_false',
            'walker' => new PrimaryNavWalker(),
        ));
    }

    public function ieTechCategoryNav()
    {
        wp_nav_menu(array(
            'container'      => false,
            'menu'           => __('Category Navigation', 'ieTech_theme'),
            'menu_class'     => 'CategoryNavigation',
            'theme_location' => 'category-nav',
            'depth'          => 0,
            'fallback_cb'    => '__return_false'
        ));
    }

    public function ieTechSocialNav()
    {
        wp_nav_menu(array(
            'container'      => false,
            'menu'           => __('Social Navigation', 'ieTech_theme'),
            'menu_class'     => 'SocialNavigation',
            'theme_location' => 'social-nav',
            'depth'          => 1,
            'fallback_cb'    => '__return_false',
            'walker'         => new SocialNavWalker(),
        ));
    }

    public function ieTechFooterNav()
    {
        wp_nav_menu(array(
            'container'      => false,
            'menu'           => __('Footer Navigation', 'ieTech_theme'),
            'menu_class'     => 'FooterNavigation',
            'theme_location' => 'footer-nav',
            'depth'          => 0,
            'fallback_cb'    => '__return_false'
        ));
    }

    public function registerPrimaryNav()
    {
        register_nav_menu('primary-nav', __('Primary Navigation', 'ieTech_theme'));
        register_nav_menu('category-nav', __('Category Navigation', 'ieTech_theme'));
        register_nav_menu('social-nav', __('Social Navigation', 'ieTech_theme'));
        register_nav_menu('footer-nav', __('Footer Navigation', 'ieTech_theme'));
    }
}
